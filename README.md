please follow these instructions to run this project:

- create virtualenv in project dir `virtualenv --python=python3.6 .venv`
- activate .venv `source .venv/bin/activate` 
- install req.txt file `pip install -r req.txt`
- run command `python manage.py migrate`
- run command `python manage.py createcachetable` (for storing forget-password tokens)
- run command `python manage.py runserver`
- put global vars in .env file (constants) => same like I had put before
- you may have to run `chmod 777 db.sqlite3`
=========
REGISTER USER
localhost/api/users/register/ => to register user (no authentication needed)
    body = {
    	"username": "username_example",
    	"first_name": "first_name_example",
    	"last_name": "last_name_example",
    	"email": "email_example",
    	"mobile": "phone_example",
    	"password": "password_example"
    }
--------------
LOGIN
localhost/api/users/login/ => to register user (no authentication needed)
    body = {
    	"username": "username_example",
    	"password": "password_example"
    }
    return value => token put it in headers 
--------------
GET USER PROFILE
localhost/api/users/id/ => to register user (authentication needed)
-------------
FORGET PASSWORD
localhost/api/users/forget-password/ => to send forget password request and get token sent by email (no authentication needed)
    body = {
    	"username": "username_example"
    }
    return value => token in email
-------------
RESET PASSWORD
localhost/api/users/reser-password/ => to reset password using token sent in email before (no authentication needed)
    body = {
    	"token": "in email and it's temp stored in cache for a period time",
    	"password": "new_password_example",
    	"confirm_password": "new_password_example",
    }
    return value => password changet
---------------
CALLING NASA API (instead of google maps api because service was down)
localhost/api/nasa/ (authentication needed)
-----------------
GET USER BROWSER INFO
localhost/api/users/browser-info/(authentication needed)
--------------------

FRONT END:
static pages for profile and login and reset password and register
